import {showMsg} from './showmsg';
import {signUp} from './authmanagement'

export default {
    signUp,
    showMsg
};